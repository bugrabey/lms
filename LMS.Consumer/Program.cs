﻿using LMS.BusinessLogic;
using LMS.Data;
using LMS.Shared;
using System;
using System.Threading;

namespace LMS.Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            string queueName = "LMS-QUEUE";
            LMSConsumer consumer = new LMSConsumer(new CourseService
                (new CourseDataAccess(MsSqlConfiguration.GetInstance()),
                new RabbitMQProducer(RabbitMQConfiguration.GetInstance())),
                RabbitMQConfiguration.GetInstance());
            CancellationTokenSource source = new CancellationTokenSource();

            var task = consumer.StartAsync(queueName, source.Token);

            Console.Read();
        }
    }
}
