﻿using LMS.BusinessLogic;
using LMS.BusinessLogic.Model.DataTransferObjects;
using LMS.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Consumer
{
    public class LMSConsumer : RabbitMQConsumer
    {
        private readonly ICourseService courseService;

        public LMSConsumer(ICourseService courseService, RabbitMQConfiguration configuration) : base(configuration)
        {
            this.courseService = courseService ?? throw new ArgumentNullException(nameof(courseService));
        }
        public override void OnReceived(string message)
        {
            try
            {
                var request = JsonConvert.DeserializeObject<SaveCourseActivityLogRequest>(message);
                courseService.SaveCourseActivityLog(request);
            }
            catch (Exception ex)
            {
                //TODO:Handle exception
                // throw;
            }

        }
    }
}
