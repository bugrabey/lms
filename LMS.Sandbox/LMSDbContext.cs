﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LMS.Sandbox
{
    public class LMSDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public LMSDbContext(DbContextOptions options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>().HasKey(k => k.Id);
        }
    }
}
