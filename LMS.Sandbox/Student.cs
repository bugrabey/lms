﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Sandbox
{
    public class Student
    {
        public int Id { get; set; }
        public DateTime BeginDate { get; set; }
    }
}
