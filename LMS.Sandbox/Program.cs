﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            TestRabbitMQConnection();
            TestRabbitMQConsumer();
            TestRabbitMQConnection();


            TestMssqlConnection();
         //   TestRedisConnection();
            Console.ReadLine();
        }

        private static Task TestMssqlConnection()
        {
            return Task.Run(() =>
           {
               using SqlConnection connection = new SqlConnection("Server=127.0.0.1\\BUGRA,1401;Database=MASTER;User Id=sa;Password=bugrabey_password;");
               connection.Open();
               Console.WriteLine("MSSQL -> OK");
           })
 ;
        }

        //private static void TestRedisConnection()
        //{
        //    var redis = StackExchange.Redis.ConnectionMultiplexer.Connect("127.0.0.1:6379");
        //    var db = redis.GetDatabase();

        //    db.StringSet("key", "value");
        //    var value = db.StringGet("key");
        //    Console.WriteLine("Redis -> OK");
        //}

        private static void TestRabbitMQConnection()
        {

            ConnectionFactory factory = new ConnectionFactory() { HostName = "localhost" };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare("LMS-QUEUE", durable: true, exclusive: false, autoDelete: false, arguments: null);
            var message = new SampleMessage { Name = "Buğra", Surname = "İlbeyi", Timestamp = DateTime.Now };
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));
            channel.BasicPublish("", "LMS-QUEUE", null, body);
            Console.WriteLine("Rabbit MQ -> OK");

        }

        private static void TestRabbitMQConsumer()
        {

            var factory = new ConnectionFactory() { HostName = "127.0.0.1" };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare("LMS-QUEUE", durable: true, exclusive: false, autoDelete: false, arguments: null);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("Message received: " + message);
            };
            channel.BasicConsume("LMS-QUEUE", autoAck: true, consumer);
            Console.WriteLine("Rabbit MQ -> OK");

        }

       

        public class SampleMessage
        {
            public string Name { get; set; }
            public string Surname { get; set; }
            public DateTime Timestamp { get; set; }
        }
    }
}
