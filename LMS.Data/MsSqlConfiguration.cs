﻿using LMS.Shared.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Data
{
    public sealed class MsSqlConfiguration
    {
        private static object locker = new object();
        private static MsSqlConfiguration istance = null;

        public string ConnectionString { get; private set; }

        

        private MsSqlConfiguration()
        {
            ConnectionString = ReadConfigValue("ConnectionString");
        }
        private string ReadConfigValue(string key)
        {
            var configuration = ConfigurationProvider.Create();
            return configuration.GetSection("MSSQLOptions")[key];
        }

        public static MsSqlConfiguration GetInstance()
        {
            if (istance == null)
            {
                lock (locker)
                {
                    if (istance == null)
                    {
                        istance = new MsSqlConfiguration();
                    }
                }
            }
            return istance;
        }
    }
}