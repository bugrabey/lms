﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Data
{
    public interface IConnectionString
    {
        public string Text { get; set; }
    }
}
