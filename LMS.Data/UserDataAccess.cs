﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace LMS.Data
{
    public class UserDataAccess
    {
        public const string InsertSPName = "Insert_User";
        private readonly MsSqlConfiguration configuration;

        public UserDataAccess(MsSqlConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public void InsertUser(string username,  string name, string surname)
        {
            using var connection = new SqlConnection(configuration.ConnectionString);
            using var command = connection.CreateCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = InsertSPName;
            command.Parameters.Add(new SqlParameter("@username", username));
            command.Parameters.Add(new SqlParameter("@name", name));
            command.Parameters.Add(new SqlParameter("@surname", surname));
            connection.Open();
            command.ExecuteNonQuery();
        }


    }
}
