﻿using LMS.Data.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Data.Mapping
{
    internal static class CourseMapping
    {
        public static IEnumerable<Course> Map(IDataReader reader)
        {
            while (reader.Read())
            {
                yield return ToCourse(reader);
            }
        }

        public static Course ToCourse(IDataReader reader)
        {
            return new Course()
            {
                Id = Convert.ToInt32(reader["CourseId"]),
                Name = reader["Name"].ToString()
            };
        }
    }
}
