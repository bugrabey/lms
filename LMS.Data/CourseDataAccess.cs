﻿using LMS.Data.Mapping;
using LMS.Data.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Data
{
    public class CourseDataAccess
    {
        public const string GetCourseSpName = "Get_assinged_courses";
        public const string InsertActivityLogSpName = "Insert_CourseActivityLog";

        private readonly MsSqlConfiguration configuration;

        public CourseDataAccess(MsSqlConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public void InserActivityLog(int userId, int courseId, int progress)
        {
            using var connection = new SqlConnection(configuration.ConnectionString);
            using var command = connection.CreateCommand();

            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = InsertActivityLogSpName;
            command.Parameters.Add(new SqlParameter("@userId", userId));
            command.Parameters.Add(new SqlParameter("@courseId", courseId));
            command.Parameters.Add(new SqlParameter("@progres", progress));
            connection.Open();
            command.ExecuteNonQuery();
        }
        public IEnumerable<Course> GetCourses()
        {
            using var connection = new SqlConnection(configuration.ConnectionString);
            using var command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = GetCourseSpName;
            connection.Open();
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                yield return CourseMapping.ToCourse(reader);
            }
            reader.Close();
        }
    }
}
