﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.UI.Models
{
    public class UserCoursesViewModel
    {
        public UserCoursesViewModel()
        {
            Courses = new List<UserCoursesItem>();
        }

        public List<UserCoursesItem> Courses { get; set; }
    }

    public class UserCoursesItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
    }
}
