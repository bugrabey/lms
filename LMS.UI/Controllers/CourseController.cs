﻿using LMS.BusinessLogic;
using LMS.BusinessLogic.Model.DataTransferObjects;
using LMS.UI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.UI.Controllers
{
    public class CourseController : Controller
    {
        private readonly ICourseService courseService;

        public CourseController(ICourseService courseService)
        {
            this.courseService = courseService ?? throw new ArgumentNullException(nameof(courseService));
        }

        [HttpGet]
        public IActionResult Index(int userId = 1)
        {
            var items = courseService.GetCoursesByUser(new GetCoursesByUserRequest()
            {
                UserId = userId
            });

            var model = new UserCoursesViewModel()
            {
                Courses = items.Courses.Select(x => new UserCoursesItem()
                {
                    Id = x.Id,
                    Link = "wwww.takelesson.com",
                    Name = x.Name
                }).ToList()
            };

            return View(model);
        }
    }
}
