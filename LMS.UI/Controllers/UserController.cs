﻿using LMS.BusinessLogic;
using LMS.BusinessLogic.Model.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.UI.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }


        [HttpGet]
        public IActionResult Index()
        {
            var model = new CreateUserRequest()
            {
                Username = "b.ilbeyi@gmail.com",
                Name = "Buğra",
                Surname = "İlbeyi"
            };
            return View(model);
        }

        [HttpPost("create")]
        public IActionResult Create([FromForm] CreateUserRequest request)
        {
            userService.CreateUser(request);
            return RedirectToAction("index","course");
        }

    }
}
