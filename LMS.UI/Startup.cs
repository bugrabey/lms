using LMS.BusinessLogic;
using LMS.Data;
using LMS.Shared;
using LMS.Shared.Caching;
using LMS.Shared.Extensions;
using LMS.Shared.TaskManagement;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.UI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();


            services.AddScoped<ITaskManager, TaskManager>();
            services.AddScoped<IStartupTask, DataSeedStartupTask>();

            services.AddScoped<ICacheService, RedisCacheService>();

            services.AddScoped<IProducer, RabbitMQProducer>();

            services.AddScoped(typeof(CourseDataAccess));
            services.AddScoped(typeof(UserDataAccess));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ICourseService, CourseService>();

            services.AddSingleton(MsSqlConfiguration.GetInstance());
            services.AddSingleton(RabbitMQConfiguration.GetInstance());
            services.AddSingleton(RedisConfiguration.GetInstance());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });


            app.ExecStartupTasks(new System.Threading.CancellationToken());
        }
    }
}
