﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.BusinessLogic.Model.DataTransferObjects
{
    public class CreateUserRequest
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
       

    }
    public class CreateUserResponse
    {
    }
}
