﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.BusinessLogic.Model.DataTransferObjects
{
    public class SaveCourseActivityLogRequest
    {
        public int UserId { get; set; }
        public int CourseId { get; set; }
        public int Progress { get; set; }
    }

    public class SaveCourseActivityLogResponse
    {
    }
}
