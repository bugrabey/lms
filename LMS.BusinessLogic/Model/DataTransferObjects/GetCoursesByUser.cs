﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.BusinessLogic.Model.DataTransferObjects
{
    public class GetCoursesByUserRequest
    {
        public int UserId { get; set; }
    }
    public class GetCoursesByUserResponse
    {
        public GetCoursesByUserResponse()
        {
            Courses = new List<Course>();
        }
        public List<Course> Courses { get; set; }
    }

    public class Course
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
