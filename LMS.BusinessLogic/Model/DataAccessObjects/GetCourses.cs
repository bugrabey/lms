﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.BusinessLogic.Model.DataAccessObjects
{
    public class GetCourses
    {

        public List<CourseItem> Courses { get; set; }
    }

    public class CourseItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
