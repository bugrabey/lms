﻿using LMS.BusinessLogic.Model.DataTransferObjects;
using LMS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.BusinessLogic
{
    public class UserService : IUserService
    {
        private readonly UserDataAccess userDataAccess;

        public UserService(UserDataAccess userDataAccess)
        {
            this.userDataAccess = userDataAccess ?? throw new ArgumentNullException(nameof(userDataAccess));
        }
        public CreateUserResponse CreateUser(CreateUserRequest request)
        {
            userDataAccess.InsertUser(request.Username, request.Name, request.Surname);
            return new CreateUserResponse();
        }


    }
}
