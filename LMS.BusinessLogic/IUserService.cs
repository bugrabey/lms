﻿using LMS.BusinessLogic.Model.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.BusinessLogic
{
    public interface IUserService
    {
        CreateUserResponse CreateUser(CreateUserRequest request);
    }
}
