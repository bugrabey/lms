﻿using LMS.BusinessLogic.Model.DataTransferObjects;
using LMS.Data;
using LMS.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.BusinessLogic
{
    public class CourseService : ICourseService
    {
        private readonly CourseDataAccess courseDataAccess;
        private readonly IProducer producer;
        private readonly string queueName = "LMS-QUEUE";
        public CourseService(CourseDataAccess courseDataAccess, IProducer producer)
        {
            this.courseDataAccess = courseDataAccess ?? throw new ArgumentNullException(nameof(courseDataAccess));
            this.producer = producer ?? throw new ArgumentNullException(nameof(producer));
        }
        public GetCoursesByUserResponse GetCoursesByUser(GetCoursesByUserRequest request)
        {
            var items = courseDataAccess.GetCourses();
            return new GetCoursesByUserResponse()
            {
                Courses = items.Select(s => new Course() { Id = s.Id, Name = s.Name }).ToList()
            };
        }

        public SaveCourseActivityLogResponse SaveCourseActivityLog(SaveCourseActivityLogRequest request)
        {
            courseDataAccess.InserActivityLog(request.UserId, request.CourseId, request.Progress);
            return new SaveCourseActivityLogResponse();
        }

        public SaveCourseActivityLogResponse SaveQueuedCourseActivityLog(SaveCourseActivityLogRequest request)
        {
            producer.Put(queueName, request);
            return new SaveCourseActivityLogResponse();
        }
    }
}
