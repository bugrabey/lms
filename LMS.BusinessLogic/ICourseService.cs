﻿using LMS.BusinessLogic.Model.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.BusinessLogic
{
    public interface ICourseService
    {
        GetCoursesByUserResponse GetCoursesByUser(GetCoursesByUserRequest request);
        SaveCourseActivityLogResponse SaveCourseActivityLog(SaveCourseActivityLogRequest request);
        SaveCourseActivityLogResponse SaveQueuedCourseActivityLog(SaveCourseActivityLogRequest request);
        
    }
}
