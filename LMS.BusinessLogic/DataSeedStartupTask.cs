﻿using LMS.Shared;
using LMS.Shared.Caching;
using LMS.Shared.TaskManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.BusinessLogic
{
    public class DataSeedStartupTask : IStartupTask
    {
        private readonly ICourseService courseService;
        private readonly ICacheService cacheService;
        private string cacheFormat = "CoursesByUser{0}";

        public DataSeedStartupTask(ICourseService courseService, ICacheService cacheService)
        {
            this.courseService = courseService ?? throw new ArgumentNullException(nameof(courseService));
            this.cacheService = cacheService ?? throw new ArgumentNullException(nameof(cacheService));
        }
        public void Execute()
        {
            var items = courseService.GetCoursesByUser(new Model.DataTransferObjects.GetCoursesByUserRequest()
            {
                UserId = 1
            });
            var cacheKey = string.Format(cacheFormat, 1);
            cacheService.Set(cacheKey, items);
        }

        public Task ExecuteAsync(CancellationToken token)
        {
            return Task.Run(() => Execute());
        }
    }
}
