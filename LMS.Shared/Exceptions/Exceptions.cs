﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Shared.Extensions
{
    public class LMSExceptions : Exception
    {
        public LMSExceptions()
        {
        }

        public LMSExceptions(string message) : base(message)
        {
        }

        public LMSExceptions(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LMSExceptions(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
