﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.Shared.TaskManagement
{
    public class TaskManager : ITaskManager
    {
        private readonly IEnumerable<IStartupTask> startupTasks;
        public TaskManager()
        {

        }
        public TaskManager(IEnumerable<IStartupTask> startupTasks)
        {
            this.startupTasks = startupTasks;
        }
        public async Task ExecuteAllAsync(CancellationToken token)
        {
            foreach (var task in startupTasks)
            {
                if (!token.IsCancellationRequested)
                    await task.ExecuteAsync(token);
            }
        }
    }
}
