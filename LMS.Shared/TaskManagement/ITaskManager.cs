﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.Shared.TaskManagement
{
    public interface ITaskManager
    {
        Task ExecuteAllAsync(CancellationToken token);
    }
}
