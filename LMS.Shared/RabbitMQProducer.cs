﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Shared
{
    public class RabbitMQProducer : IProducer
    {
        private readonly RabbitMQConfiguration configuration;

        public RabbitMQProducer(RabbitMQConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }
        public void Put<T>(string queueName, T instance) where T : class
        {
            ConnectionFactory factory = new ConnectionFactory()
            {
                HostName = configuration.Hostname,
                UserName = configuration.Username,
                Password = configuration.Password
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(instance));
            channel.BasicPublish("", queueName, null, body);
        }
    }
}
