﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.Shared
{
    public interface IConsumer
    {
        Task StartAsync(string queueName, CancellationToken token);
        void OnReceived(string message);
    }
}
