﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.CommandLine;
using Microsoft.Extensions.Configuration.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Shared
{
    public sealed class RabbitMQConfiguration
    {
        public string Hostname { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }

        private static object locker = new object();
        private static RabbitMQConfiguration instance = null;

        private RabbitMQConfiguration()
        {
            Hostname = ReadConfigValue("HostName");
            Username = ReadConfigValue("Username");
            Password = ReadConfigValue("Password");
        }

        private string ReadConfigValue(string key)
        {
            var configuration = LMS.Shared.Configuration.ConfigurationProvider.Create();
            return configuration.GetSection("RabbitMQOptions")[key];
        }
        public static RabbitMQConfiguration GetInstance()
        {
            if (instance == null)
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new RabbitMQConfiguration();
                    }
                }
            }
            return instance;
        }
    }
}
