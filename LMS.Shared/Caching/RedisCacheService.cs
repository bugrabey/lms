﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Shared.Caching
{
    public class RedisCacheService : ICacheService
    {
        private readonly RedisConfiguration configuration;

        public RedisCacheService(RedisConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }
        public string Get(string key)
        {
            return GetDatabase().StringGet(key);
        }

        public void Set(string key, string value)
        {
            GetDatabase().StringSet(key, value);
        }

        public void Set<T>(string key, T instance) where T : class
        {
            var data = JsonConvert.SerializeObject(instance);
            Set(key, data);
        }

        private IDatabase GetDatabase()
        {
            var redis = StackExchange.Redis.ConnectionMultiplexer.Connect(configuration.ConnectionString);
            return redis.GetDatabase();
        }
    }
}
