﻿using LMS.Shared.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Shared.Caching
{
 
    public sealed class RedisConfiguration
    {
        public string ConnectionString { get; private set; }

        private static object locker = new object();
        private static RedisConfiguration instance = null;

        private RedisConfiguration()
        {
            ConnectionString = ReadConfigValue("ConnectionString");
        }

        private string ReadConfigValue(string key)
        {
            var configuration = ConfigurationProvider.Create();
            return configuration.GetSection("RedisOptions")[key];
        }
        public static RedisConfiguration GetInstance()
        {
            if (instance == null)
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new RedisConfiguration();
                    }
                }
            }
            return instance;
        }
    }
}
