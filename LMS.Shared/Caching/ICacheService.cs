﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Shared.Caching
{
    public interface ICacheService
    {
        void Set(string key, string value);
        string Get(string key);
        void Set<T>(string key, T instance) where T : class;
    }
}
