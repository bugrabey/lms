﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.Shared
{
    public abstract class RabbitMQConsumer : IConsumer
    {
        private readonly RabbitMQConfiguration configuration;

        public RabbitMQConsumer(RabbitMQConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }
        public abstract void OnReceived(string message);


        public Task StartAsync(string queueName, CancellationToken token)
        {
            return Task.Run(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    ReadMessage(queueName);
                    System.Threading.Thread.Sleep(1);
                }
            });
        }

        private void ReadMessage(string queueName)
        {
            var factory = new ConnectionFactory() { HostName = configuration.Hostname };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (sender, e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                OnReceived(message);
            };
            channel.BasicConsume(queueName, autoAck: true, consumer);
        }
    }
}
