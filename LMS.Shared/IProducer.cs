﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.Shared
{
    public interface IProducer
    {
        void Put<T>(string ququeName, T instance) where T : class;
    }
}
