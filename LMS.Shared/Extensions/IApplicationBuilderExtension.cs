﻿using LMS.Shared.TaskManagement;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.Shared.Extensions
{
    public static class IApplicationBuilderExtension
    {
        public static void ExecStartupTasks(this IApplicationBuilder builder, CancellationToken token)
        {
            try
            {
                var scope = builder.ApplicationServices.CreateScope();
                var manager = scope.ServiceProvider.GetService<ITaskManager>();

                manager.ExecuteAllAsync(token);
            }
            catch (Exception ex)
            {
                throw new LMSExceptions("Exception occured when startup tasks are executed!", ex);
            }
        }
    }

}
