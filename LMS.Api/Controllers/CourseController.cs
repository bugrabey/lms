﻿using LMS.BusinessLogic;
using LMS.BusinessLogic.Model.DataTransferObjects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CourseController : ControllerBase
    {
        private readonly ICourseService courseService;

        public CourseController(ICourseService courseService)
        {
            this.courseService = courseService ?? throw new ArgumentNullException(nameof(courseService));
        }
        [HttpPost]
        public IActionResult Index([FromBody] SaveCourseActivityLogRequest request)
        {
            courseService.SaveQueuedCourseActivityLog(request);
            return Ok();
        }
    }
}
